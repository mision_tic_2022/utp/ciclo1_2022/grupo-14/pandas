import pandas as pd

guion = lambda m: print(f'----------------------------{m}-------------------------------')

serie = pd.Series( [10,20,30,40], index=['Enero', 'Febrero', 'Marzo', 'Abril'] )
print(serie)
#Acceder al objeto index
print( serie.index )

guion('Sobreescribir el objeto index')
serie.index = ['ene','feb','mar','abr']
print(serie.index)

guion('Acceder a los valores')
print(serie.values)

guion('Obtener el tipo de dato')
print( serie.dtype )

guion('Crear etiquetas en la serie')
serie.index.name = 'Meses'
serie.name = 'Serie de ejemplo Misión Tic 2022 UTP'
print(serie)

guion('Acceder a las dimensiones de la serie')
print( serie.axes )
print( serie.shape )

guion('')
guion('DataFrame')
guion('')

dict_ventas = {
    'frutas': [120,500,900,250,300,650,260,950,1150,430,580,320],
    'aseo': [600,800,110,230,260,990,80,10,50,540,120,540]
}
#Crear dataframe
ventas = pd.DataFrame(dict_ventas, index=['ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic'])
print(ventas)

guion('Operación entre columnas y creación de una nueva columna')
ventas['total_ventas'] = ventas.frutas + ventas.aseo
print(ventas)

guion('Acceder al objeto que contiene los indices de las columnas')
print( ventas.columns )

guion('Acceder al index de las filas')
print(ventas.index)

guion('')
print( ventas.axes )
print( ventas.shape )

guion('')
print(ventas)

guion('info')
print( ventas.info() )

guion('describe')
print( ventas.describe() )

guion('Acceder a los primeros 5 registros')
print( ventas.head() )
guion('')
print( ventas.head(8) )

guion('Acceder a los últimos 5 registros')
print( ventas.tail() )
guion('')
print( ventas.tail(8) )

guion('Obtener una fila de manera aleatoria')
print( ventas.sample() ) 

print( ventas.sample(5) )

guion('Acceder a un valor en específico')
ventas_frutas_may = ventas['frutas']['may']
print(ventas_frutas_may)
guion('')
print( ventas['frutas'][4] )

guion('Seleccionar registros')
#print( ventas[ ventas['total_ventas'] > 1000 ] ) 
print( ventas[ ventas.total_ventas > 1000 ] ) 