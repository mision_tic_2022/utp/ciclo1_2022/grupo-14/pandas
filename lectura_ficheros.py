import pandas as pd

guion = lambda : print('------------------------------------------')
#Cargar el fichero como dataframe
movies = pd.read_csv('movies.csv')

print( movies.info() )

guion()

print( movies.describe() )

guion()

print( movies.head(10) )

guion()
print( movies.tail() )

guion()

print( movies.columns )

guion()
guion()
datos = movies.iloc[0:2]
print(datos)

guion()
guion()

subDataframe = movies.loc[:, ['Title','Director','Facebook likes - Movie', 'Gross Earnings', 'Budget']]

subDataframe['Gross Earnings'].fillna(0, inplace=True)
subDataframe['Budget'].fillna(0, inplace=True)

print( subDataframe.info() )
guion()
print(subDataframe.describe())
guion()
print(subDataframe.head(100))

