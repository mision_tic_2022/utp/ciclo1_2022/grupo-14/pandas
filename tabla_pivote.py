import pandas as pd
import numpy as np

def procesar_peliculas(rutaArchivo: str):
    movies = pd.read_csv(rutaArchivo)
    #Obtener subDataframe
    subDataframe = movies.loc[:, ['Title','Director','Facebook likes - Movie', 'Gross Earnings', 'Budget']]
    #print( subDataframe.head() )
    #Crear tabla pivote/ tabla dinámica
    #tabla = pd.pivot_table(subDataframe, index=['Director'], aggfunc=[np.sum, np.size, np.mean])
    
    peliculas_populares = subDataframe[ subDataframe['Facebook likes - Movie'] >= 100000 ]
    print(peliculas_populares)

#procesar_peliculas('movies.csv')

def analizar_casos_covid(ruta: str):
    casos_covid = pd.read_csv(ruta)
    print( casos_covid.info() )
    print(casos_covid.describe())
    print( casos_covid['País de procedencia'].head() )
    print( casos_covid['País de procedencia'].tail() )
    print( casos_covid['País de procedencia'].sample(20) )

analizar_casos_covid('casos_covid.csv')